var express = require('express');
var config = require('./config.js');
var request = require("request");
var querystring = require("querystring");
var bodyParser = require('body-parser');
var path = require("path");
var geoip = require('geoip-lite');
var google = require('googleplaces');
var app = express();


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use("/", express.static(__dirname + '/public'));

// This responds send index file  on the home page
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/index.html'));
});

//  This responds a POST request for the homepage page
app.post('/', function (req, res) {
    var str = req.body.Latitude + ',' + req.body.Longitude;
    res.redirect('/channel?l=' + str);
});

//  This responds a POST request for the channel route and send the json response
app.get('/channel', function (req, res) {
    var ip = req.headers['x-forwarded-for'] ||
             req.connection.remoteAddress ||
             req.socket.remoteAddress ||
             req.connection.socket.remoteAddress;
    var coordinates;

    if (req.query && req.query.l){
        var arr = req.query.l.split(',');
        if(arr.length === 2 && arr[0] != '' && arr[1] != ''){
            findChannel(arr,res);
        } else {
            findloc(ip,function(result){
                coordinates  = result.ll;
                findChannel(coordinates,res);
            });
        }
    } else {
        findloc(ip,function(result){
            coordinates  = result.ll;
            findChannel(coordinates,res);
        });
    }
});

// Bind it at port 5000 using listen method.
var server = app.listen(5000, function () {
    console.log('Example app listening at 5000');
});

// this function is used for find the latitude and longitude
function findloc (ip, callback){
    console.log("ip" , ip);
    var geo_location = geoip.lookup(ip);
    console.log("location" , geo_location);
    callback(geo_location);
}

// this function is used for the get the json response  within 500 radius

function findChannel(coordinate,res){
    var parameters = {
        key: config.apiKey,
        location: coordinate.toString(),
        types: "hospital",
        radius: 500
    };
    var options = "https://maps.googleapis.com/maps/api/place/search/" + config.outputFormat + "?" + querystring.stringify(parameters);
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.send(body); // Send  the JSON for the channel route. 
        }else{
            res.send(error);
        }
    })

}